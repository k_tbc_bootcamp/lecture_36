package com.university.exam8.bean

import com.google.gson.annotations.SerializedName

class NewsModel {

    var id = 0
    var descriptionEN: String = ""
    var descriptionKA: String = ""
    var descriptionRU: String = ""
    var titleEN: String = ""
    var titleKA: String = ""
    var titleRU: String = ""

    @SerializedName("created_at")
    var createdAt: Long = 0

    @SerializedName("updated_at")
    var updatedAt: Long = 0
    var cover: String = ""
    var isLast = false

}